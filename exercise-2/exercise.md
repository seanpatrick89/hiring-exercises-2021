### **Brief**

1 - Answer these questions about the design of this [landing](https://logtail.com/)

a) What makes this a good design? What elements? What decisions?

The layout is well thought out - the logo is well placed and immediately the users will take note of the branding. There is a catchy headline that conveys the most valuable benefit for your audience. There are many CTA buttons sprinkled through the page, yet they never seem intrusive. The placement of these is well designed. In the beyond the fold area you benefits of the Logtail listed which keeps the visitor engaged as the scroll down the screen. The animations for the details to come in are smooth and look very professional and clean. There is harmony in the design. The elements fit together and the color scheme is maintained. The consistency in colors and page design encourages the visitor to stay on the site. The copy is short and to the point, and personalizes the message of the company to the visitor. 

b) How do you think was the process of the designers to get to this result?

I imagine there is a very specific approach the designers take in implementing a new design. Perhaps I would invision something like this:
a. Identify the goals the new website need to fulfill and what the purpose is.
b. Figure out the scope of the project, the different web pages are needed, and come up with user stories.
c. Wireframe. Now figure out the features for each page and define how everything is going to interrelate. 
d. Work with real content. It's best to design pages using real content.
e. Focus on the branding, the colors, create a moodboard, and develop a logo. 
f. Test the site with users and identify any issues with UX. 


2 - Using Figma, create a mood board about the landing. Represent these three aspects:

a) Color palette

b) Illustrations style (related in some way to the landing)

c) Typography (find one similar in Google fonts if the original is not free)

### **Important clarifications**

- These 2 points should take you less than an hour.
- The mood board should not be a big design project, it is just to demonstrate you have a well understanding of Figma as a tool. Think of it as a sketching task that should be done in around half an hour, it's shouldn't be pixel perfect.

Link to figma proyect: //paste it here

### Steps

1 ) Fork the repository [https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/](https://bitbucket.org/wildaudience/hiring-exercises-2021/src/master/) (same as exercise 1)

2. Inside `exercise-2/exercise.md` write down the answers to point 1 and paste link to the figma project of point 2.

3. Commit the changes and push to master. Share the link to the repo and send an invite to the Figma project to `l@wildaudience.com`

Figma will also send invite - 

https://www.figma.com/proto/OP4seo1g3nm5UiqvVRW4aX/wild-audience-mood-board?page-id=0%3A1&node-id=0%3A2341&viewport=241%2C48%2C0.12&scaling=min-zoom