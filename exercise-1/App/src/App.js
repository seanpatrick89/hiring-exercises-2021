import React from 'react'
import User from './components/User'
import Posts from './components/Posts'
import Layout from './components/Layouts/Layout'

function App() {
  return (
    <Layout>
      <User />
      <Posts />
    </Layout>
  )
}

export default App
