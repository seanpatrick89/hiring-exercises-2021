import React from 'react'
import { useQuery, gql } from '@apollo/client'
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import CardLayout from './Layouts/CardLayout'

const GET_USER = gql`
  {
    user(id: 1) {
      username
      posts(options: { paginate: { page: 1, limit: 10 } }) {
        data {
          id
          title
        }
      }
    }
  }
`

function Posts() {
  const { loading, error, data } = useQuery(GET_USER)
  if (loading) return <Loading />
  if (error) return <ErrorComponent error={error} />
  const posts = data.user.posts.data

  return (
    <CardLayout>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
        {data.user.username}'s Posts:
      </p>
      {posts.map((post) => (
        <p
          key={post.id}
          className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start"
        >
          Title: {post.title}
        </p>
      ))}
    </CardLayout>
  )
}

export default Posts
