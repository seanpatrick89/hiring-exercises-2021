import React from 'react'
import { useQuery, gql } from '@apollo/client'
import Loading from './Loading'
import ErrorComponent from './ErrorComponent'
import UserLayout from './Layouts/UserLayout'

const GET_USER = gql`
  {
    user(id: 1) {
      id
      username
      email
      phone
      company {
        name
      }
      address {
        street
        city
        suite
      }
    }
  }
`

function User() {
  const { loading, error, data } = useQuery(GET_USER)
  if (loading) return <Loading />
  if (error) return <ErrorComponent error={error} />

  const { address, company, email, phone, username } = data.user
  return (
    <UserLayout>
      <h1 className="text-3xl font-bold pt-8 lg:pt-0">{username}</h1>
      <div className="mx-auto lg:mx-0 w-4/5 pt-3 border-b-2 border-teal-500 opacity-25"></div>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start">
        {' '}
        Address:
      </p>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        Street Suite City
        {address.street} {address.suite} {address.city}
      </p>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
        Email:
      </p>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        {email}
      </p>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
        Phone:
      </p>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        {phone}
      </p>
      <p className="pt-4 text-base font-bold flex items-center justify-center lg:justify-start mt-4">
        Company:
      </p>
      <p className="pt-2 text-gray-600 text-xs lg:text-sm flex items-center justify-center lg:justify-start">
        {company.name}
      </p>
    </UserLayout>
  )
}

export default User
