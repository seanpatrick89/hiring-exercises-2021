import React from 'react'

function UserLayout({ children }) {
  return (
    <div className="w-full lg:w-3/5 rounded-lg lg:rounded-l-lg lg:rounded-r-none shadow-2xl bg-white opacity-75 mx-6 lg:mx-0">
      <div className="p-4 md:p-12 text-center lg:text-left">{children}</div>
    </div>
  )
}

export default UserLayout
