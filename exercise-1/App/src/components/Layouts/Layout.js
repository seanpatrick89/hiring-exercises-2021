import React from 'react'

function Layout({ children }) {
  return (
    <div
      id="body"
      className="font-sans antialiased text-gray-900 leading-normal tracking-wider bg-cover p-12"
      style={{
        backgroundImage: "url('https://source.unsplash.com/1L71sPT5XKc')",
      }}
    >
      <main className="max-w-4xl flex items-center h-auto lg:h-screen flex-wrap mx-auto my-32 lg:my-0">
        {children}
      </main>
    </div>
  )
}

export default Layout
